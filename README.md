# Please Read This

## About Project

**PHP Version 7.3** : As mentioned in job AD (PHP 7 is required) I used php version 7.3. 
It could be built with php 8 or 8.1 Since these are the latest versions that 
have been released lately and have a lot of cool features. 

So my PHP version choice is based on the job Ad and not preference.

This project is a PXL assessment and this readme is provided to guide you through the structure of the project and how to run
it.

The target JSON file is located here: `storage/app/importable/challenge.json`

**IMPORTANT:** There are 10001 records that exist in the `challenge.json` file, 
but with the validations that applied to the file items, 
there will be 611 records imported to the database. So don't be surprised.

## How to run and Test

The main logic is located in a job class that is called `AccountsJsonFileImportJob`, so that the process can be handled in the background by a queue.

This job can be dispatched from `ImportChallengeJsonCommand`. In order to run this command, copy this command
in your terminal:

```shell
php artisan system:import:challenge-json
```

Note that if your queue driver is set to anything other than `sync` the process will be queued, and
you need a queue worker to execute it.

Feel free to change the readers, importers, and filters in the `AccountsJsonFileImportJob`.

**Note:** The `ImportChallengeJsonCommand` is also added to laravel scheduler since it was mentioned
in the assessment file (Run in background).

## Structure

### Models & Migrations

We have 2 models and their corresponded migrations:

1. Account: The information about an account
2. CreditCard: The credit card info of an account

### FileReader & FileImporter

There are 2 main modules that are used for reading the file and importing it to the database.

1. FileReaderService
2. FileImportService

#### 1.FileReader

**Structure:**

This Module is located under the `Services` directory. The main design pattern that is used for file readers is 
`Strategy` pattern. We have readers that implement `FileReaderContract`. The file readers can be switched at any time
in the `FileReaderService` context.

The file Reader must return a `\Generator` object. in order to avoid memory limitations.

**Pointers:**

The Pointers are built using the `Singletone` design pattern.
A file reader must take a pointer class that implements `PointerContract`. These pointers are used for tracking the file
reader pointer and then resuming it if the execution is interrupted somehow.

I used cache driver for saving pointers state, There are 4 types of pointers are created:

1. `NormalPointer`: It Doesn't save the pointer's state and always starts from beginning of the file
2. `FileCachePointer`: It uses file cache driver for saving pointer's state
3. `RedisCachePointer`: It uses redis cache driver for saving pointer's state
4. `DatabaseCachePointer`: It uses database cache driver for saving pointer's state (Don't forget to add cache table)

You can implement your own pointer. 
All you have to do is to implement the `PointerContract` or `CachePointerContract` and pass it to the reader.

---
**Readers:**

There are 3 readers built for this assessment in the `App\Services\FileRead\Readers` namespace:

* `CsvReader`: Although you did not ask for a CSV reader I implemented it for a demo, and it works
* `JsonReader`: The main reader that is used to read the JSON file of the assessment
* `XmlReader`: The XML reader is not implemented since not asked in challenge docs but here is a demo of it

Any reader can be added later if the clients demand for more. By only adding a reader class that
implements `ReaderStrategyContract` and pass the new file reader to `FileReaderService`.

#### 2.FileImporter

All the code is handled with `FileImportService`.

This class accepts 3 contracts.
1. `FileReaderContract` : used for reading a file
2. `ImporterContract` : used for importing the file
3. `DataManipulatorPipelineContract` : this is built based on `Chain of Responsibility` design pattern for manipulating
   data before insertion

**Importers**

All the importers must implement `ImporterContract`. This contract has
2 methods:
1. `prepare`: is used for preparing data before insertion
2. `import`: is used for importing the data

The importers are located under `App\Services\FileImport\Importers`.
So far 2 importers are implemented:
1. `BulkAccountImporter`: It inserts data in bulk mode to avoid multiple queries.
   this Importer is good for large files (max execution time is below 20 seconds for 2.5 Mb files)
2. `SingleAccountImporter`: It inserts data one by one. This class is only implemented for demo purposes and is not good for large files

**Data Manipulators**

If you want to manipulate, validate, or filter data before data insertion. Just make a class with
`DataManipulatorContract` implemented. Then pass the class to the `DataManipulator` pipeline.

In this scenario, there are 4 data manipulators implemented based on the assessment pdf.
1. `AccountJsonDataManipulator`: This class change the structure of the JSON and adds some additional data like UUID, in order
   to make sure that the data will be unique in the database
2. `AccountUniquenessValidator`: This file uses UUID to validate that if the data does not exist before
3. `AgeValidator`: this file validates that if the account owner's age is between 18 and 65
4. `CreditCardValidator`: this file validates that if the credit card number contains three of the same numbers
   consecutively

These data manipulators are passed through a pipeline to `FileImporterService`

**How was the data duplication problem solved?**

Well, the answer is UUID. Since we don't want to miss any data and make sure that
the data that will be inserted is unique and in the exact order of JSON file, 
I created a UUID version 3 based on the JSON value of each item and then checked if 
the UUID is inserted before or not.

Even if we execute the file over and over again either from beginning or middle of the file, 
the data will always be unique.
