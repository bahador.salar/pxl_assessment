<?php

namespace App\Services\FileRead\Contracts;

interface PointerContract
{
    public function get(): int;

    public function initialize(): void;

    public function update(int $pointer): void;

    public function reset(): void;

}
