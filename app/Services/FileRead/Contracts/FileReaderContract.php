<?php

namespace App\Services\FileRead\Contracts;

interface FileReaderContract
{
    public function read(): \Generator;

    public function close(): bool;
}
