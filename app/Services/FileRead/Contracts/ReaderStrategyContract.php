<?php

namespace App\Services\FileRead\Contracts;

/**
 * The Strategy interface declares operations common to all supported versions
 * of reading algorithm.
 *
 * The Context (FileReaderService) uses this interface to call the algorithm defined by Concrete
 * Strategies (Readers).
 */
interface ReaderStrategyContract
{
    /**
     * @param string $path the file full path
     * @param PointerContract $pointer the pointer to that tracks the file read
     */
    public function __construct(string $path, PointerContract $pointer);

    /**
     * reads the file stream
     * @return \Generator
     */
    public function read(): \Generator;

    /**
     * closed the file stream
     * @return bool
     */
    public function close(): bool;
}
