<?php

namespace App\Services\FileRead\Contracts;

interface CacheablePointerContract extends PointerContract
{
    public function setKey(string $key) : void;
}
