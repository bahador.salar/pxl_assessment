<?php

namespace App\Services\FileRead\Readers;

use Ramsey\Uuid\Uuid;
use App\Services\FileRead\Contracts\PointerContract;
use App\Services\FileRead\Contracts\ReaderStrategyContract;
use App\Services\FileRead\Contracts\CacheablePointerContract;

abstract class BaseReader implements ReaderStrategyContract
{
    /**
     * @var false|resource
     */
    protected $resource;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var PointerContract
     */
    protected $pointer;

    public function __construct(string $path, PointerContract $pointer)
    {
        if (!file_exists($path)) {
            throw new \InvalidArgumentException('There is no file at given path');
        }

        $this->resource = fopen($path, 'rb');

        $this->initPointer($pointer, $path);
    }

    public function close(): bool
    {
        //reset the pointer so that the file can be read from first line again
        if ($this->pointer)
            $this->pointer->reset();

        if (is_resource($this->resource)) {
            fclose($this->resource);
            return true;
        }

        return false;
    }

    public function __destruct()
    {
        $this->close();
    }

    /**
     * @param PointerContract $pointer
     * @param string $path
     */
    private function initPointer(PointerContract $pointer, string $path): void
    {
        $this->pointer = $pointer;

        if ($this->pointer instanceof CacheablePointerContract) {
            $this->pointer->setKey(Uuid::uuid5(Uuid::NAMESPACE_OID, $path)->toString());
        }

        $this->pointer->initialize();
    }
}
