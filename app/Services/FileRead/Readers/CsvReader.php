<?php

namespace App\Services\FileRead\Readers;

use App\Services\FileRead\Contracts\PointerContract;

// Although you did not ask for a CSV reader I implemented it for a demo, and it works
class CsvReader extends BaseReader
{
    /**
     * @var array
     */
    private $titles;

    public function __construct(string $path, PointerContract $pointer)
    {
        parent::__construct($path, $pointer);

        rewind($this->resource);

        $this->titles = fgetcsv($this->resource);
    }


    public function read(): \Generator
    {
        fseek($this->resource, $this->pointer->get());

        $pointer = 0;
        while (($row = fgetcsv($this->resource)) !== false) {
            if ($row === $this->titles)
                continue;

            $this->pointer->update(++$pointer);

            yield array_combine($this->titles, $row);
        }

        $this->close();
    }
}
