<?php

namespace App\Services\FileRead\Readers;

class JsonReader extends BaseReader
{
    /**
     * chunk size of file read
     * its public so that it can be modified from outside of class
     * @var int
     */
    public static $chunkSize = 8192;

    /**
     * every file chunk will be stored in buffer
     * @var string
     */
    protected $buffer = '';

    /**
     * checks the nesting level that the pointer is currently reading
     *
     * @var int
     */
    protected $nestingLevel = 0;

    public function read(): \Generator
    {
        $this->buffer = '';
        $this->nestingLevel = 0;

        fseek($this->resource, $this->pointer->get());

        while (!feof($this->resource)) {
            $chunk = fread($this->resource, self::$chunkSize);

            yield from $this->parseChunk($chunk);
        }

        $this->close();
    }

    private function parseChunk(string $chunk): \Generator
    {
        // Continue from where we left off
        $this->buffer .= $chunk;

        $start = 0;
        $keepFrom = 0;
        $yielded = 0;

        // We want to iterate over chars but since,
        // we can have multibyte strings we can't access them by position alone
        $split = mb_str_split($this->buffer);

        foreach ($split as $position => $char) {
            // at the start of collection we should skip the char
            if ($this->isStartOfCollection($char)) {
                continue;
            }

            // At end of collection we should break
            if ($this->isEndOfCollection($char)) {
                break;
            }

            // Maybe start of an item, but we need to check if we're not nested
            if ($char === '{') {
                $this->startOfObject($position, $start);
            } elseif ($char === '}') {
                yield from $this->endOfObject($position, $keepFrom, $start, $yielded);
            }
        }

        // If we don't reset nesting level when current buffer hasn't yielded any items,
        // we'll never reach 0 and won't be able to read items from next chunks.
        if ($yielded === 0) {
            $this->nestingLevel = 0;
        }

        // Keep the unparsed part for later.
        $this->buffer = mb_substr($this->buffer, $keepFrom);
    }

    /**
     * @param int $position
     * @param int $start
     */
    private function startOfObject(int $position, int &$start): void
    {
        // Definitely start of the item
        if ($this->nestingLevel === 0) {
            $start = $position;
        }

        $this->nestingLevel++;
    }

    /**
     * @param int $position
     * @param int $keepFrom
     * @param int $start
     * @param int $yielded
     * @return \Generator
     */
    private function endOfObject(int $position, int &$keepFrom, int $start, int &$yielded): \Generator
    {
        // Maybe end of the item?
        $this->nestingLevel--;

        // Definitely end of the item
        if ($this->nestingLevel === 0) {
            $this->pointer->update($keepFrom = $position + 1);

            yield json_decode(
                mb_substr($this->buffer, $start, $position - $start + 1),
                true
            );

            $yielded++;
        }
    }

    /**
     * @param $char
     * @return bool
     */
    private function isStartOfCollection($char): bool
    {
        return $this->nestingLevel === 0 && $char === '[';
    }

    /**
     * @param $char
     * @return bool
     */
    private function isEndOfCollection($char): bool
    {
        return $this->nestingLevel === 0 && $char === ']';
    }

}
