<?php

namespace App\Services\FileRead\Pointers;

use App\Services\FileRead\Contracts\PointerContract;
use Illuminate\Contracts\Cache\Repository as CacheContract;

abstract class BaseCachePointer implements PointerContract
{
    /**
     * @var int
     */
    protected $pointer = 0;

    /**
     * @var string
     */
    private $key;

    /**
     * @var bool
     */
    protected $shouldContinue = true;

    /**
     * @var self
     */
    protected static $instance;


    private function __construct(bool $shouldContinue = true)
    {
        $this->shouldContinue = $shouldContinue;
    }

    /**
     * we make pointer singleton in order to make objects once and track the pointer
     *
     * @param bool $shouldContinue
     * @return $this
     */
    public static function init(bool $shouldContinue = true): self
    {
        if (is_null(self::$instance))
            self::$instance = new static($shouldContinue);

        return self::$instance;
    }

    public function setKey(string $key): void
    {
        $this->key = $key;
    }

    public function get(): int
    {
        return $this->getCacheInstance()->get($this->key, 0);
    }

    public function initialize(): void
    {
        if ($this->shouldContinue) {
            $this->pointer = $this->get();
        } else {
            $this->reset();
        }
    }

    public function update(int $pointer): void
    {
        $this->getCacheInstance()->put($this->key, $this->pointer += $pointer);
    }

    public function reset(): void
    {
        $this->pointer = 0;
        $this->getCacheInstance()->forget($this->key);
    }

    abstract protected function getCacheInstance(): CacheContract;

}
