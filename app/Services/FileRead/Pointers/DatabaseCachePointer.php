<?php

namespace App\Services\FileRead\Pointers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Contracts\Cache\Repository as CacheContract;

class DatabaseCachePointer extends BaseCachePointer
{
    protected function getCacheInstance(): CacheContract
    {
       return Cache::store('database');
    }
}
