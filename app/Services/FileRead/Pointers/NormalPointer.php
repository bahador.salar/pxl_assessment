<?php

namespace App\Services\FileRead\Pointers;

use App\Services\FileRead\Contracts\PointerContract;

// this pointer does not remember its previous state and always starts from zero
class NormalPointer implements PointerContract
{
    /**
     * @var int
     */
    private $pointer;

    public function __construct(int $pointer = 0)
    {
        $this->pointer = $pointer;
    }

    public function get(): int
    {
        return $this->pointer;
    }

    public function initialize(): void
    {
        $this->pointer = 0;
    }

    public function update(int $pointer): void
    {
        $this->pointer += $pointer;
    }

    public function reset(): void
    {
        $this->pointer = 0;
    }
}
