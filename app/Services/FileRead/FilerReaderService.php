<?php

namespace App\Services\FileRead;

use App\Services\FileRead\Contracts\FileReaderContract;
use App\Services\FileRead\Contracts\ReaderStrategyContract;

/**
 * The Context defines the interface of interest to clients.
 */
class FilerReaderService implements FileReaderContract
{
    /**
     * @var ReaderStrategyContract The Context maintains a reference to one of the Strategy
     * objects. The Context does not know the concrete class of a strategy. It
     * should work with all strategies via the Strategy interface.
     */
    private $reader;

    /**
     * Usually, the Context accepts a strategy through the constructor, but also
     * provides a setter to change it at runtime.
     */
    public function __construct(ReaderStrategyContract $reader)
    {
        $this->reader = $reader;
    }

    /**
     * Usually, the Context allows replacing a Strategy object at runtime.
     */
    public function setReader(ReaderStrategyContract $reader)
    {
        $this->reader = $reader;
    }

    /**
     * The Context delegates some work to the Strategy object instead of
     * implementing multiple versions of the algorithm on its own.
     */
    public function read(): \Generator
    {
        foreach ($this->reader->read() as $data){
            yield $data;
        }

       $this->close();
    }

    /**
     * The Context delegates some work to the Strategy object instead of
     * implementing multiple versions of the algorithm on its own.
     */
    public function close(): bool
    {
        return $this->reader->close();
    }
}
