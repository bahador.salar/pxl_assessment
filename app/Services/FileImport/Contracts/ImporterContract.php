<?php

namespace App\Services\FileImport\Contracts;

interface ImporterContract
{
    public function prepare(array $data) : ImporterContract;

    public function import(bool $force) : void;
}
