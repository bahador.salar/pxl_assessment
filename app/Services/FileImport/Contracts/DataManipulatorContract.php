<?php

namespace App\Services\FileImport\Contracts;

// with DataManipulator contract any filter or modification to data can be added into filter pipeline
// add will implement to data before insertion
interface DataManipulatorContract
{
    /**
     * handle method that the pipeline will call
     *
     * @param array|null $data
     * @return array|null
     */
    public function handle(?array $data) : ?array;
}
