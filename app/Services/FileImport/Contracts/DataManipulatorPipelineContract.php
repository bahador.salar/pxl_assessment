<?php

namespace App\Services\FileImport\Contracts;

interface DataManipulatorPipelineContract
{
    public function addManipulator(DataManipulatorContract $manipulator): DataManipulatorPipelineContract;

    public function apply(?array $data) : ?array;

    public function getApplied() : ?DataManipulatorContract;
}
