<?php

namespace App\Services\FileImport\DataManipulators;

use Illuminate\Support\Arr;
use App\Services\FileImport\Contracts\DataManipulatorContract;

abstract class BasedOnFieldManipulator implements DataManipulatorContract
{
    /**
     * @var string
     */
    private $attribute;

    public function __construct(string $attribute)
    {
        $this->attribute = $attribute;
    }

    /**
     * handle method that the pipeline will call
     *
     * @param array|null $data
     * @return array|null
     */
    public function handle(?array $data): ?array
    {
        if (is_array($data) && $this->isValid(Arr::get($data, $this->attribute)))
            return $data;

        return null;
    }

    /**
     * checks the if the data is valid
     *
     * @param $value
     * @return bool
     */
    abstract protected function isValid($value): bool;
}
