<?php

namespace App\Services\FileImport\DataManipulators;

// This requirement was the last item of condition section in the assessment pdf
class AgeValidator extends BasedOnFieldManipulator
{
    public function __construct(string $attribute = 'date_of_birth')
    {
        parent::__construct($attribute);
    }

    /**
     * checks if age is between 18 and 65
     *
     * @param $value
     * @return bool
     */
    protected function isValid($value): bool
    {
        if(empty($value))
            return false;

        $birthDate = parseToCarbon($value);

        if(!$birthDate)
            return false;

        $age = $birthDate->diffInYears();

        return $age >= 18 && $age <= 65;
    }
}
