<?php

namespace App\Services\FileImport\DataManipulators;

use Carbon\Carbon;
use App\Services\FileImport\Contracts\DataManipulatorContract;

class AccountJsonDataManipulator implements DataManipulatorContract
{
    public function handle(?array $data): ?array
    {
        if (empty($data))
            return null;

        if (!array_keys_exists($data, $this->accountKeys()))
            return null;

        if (!is_array($data['credit_card']) || !array_keys_exists($data['credit_card'], $this->creditCardKeys()))
            return null;

        return $this->formattedData($data);
    }

    private function accountKeys(): array
    {
        return ['name', 'address', 'description', 'interest', 'date_of_birth', 'email', 'account', 'credit_card'];
    }

    private function creditCardKeys(): array
    {
        return ['type', 'number', 'name', 'expirationDate'];
    }

    private function formattedData(array $data): array
    {
        $time = Carbon::now()->toDateTimeString();

        return [
            'name'          => $data['name'],
            'address'       => $data['address'],
            'checked'       => $data['checked'],
            'description'   => $data['description'],
            'interest'      => $data['interest'],
            'date_of_birth' => ($carbon = parseToCarbon($data['date_of_birth'])) ? $carbon->toDateString() : null,
            'email'         => $data['email'],
            'account'       => $data['account'],
            'uuid'          => $data['uuid'] ?? fileItemUuid($data),
            'created_at'    => $time,
            'updated_at'    => $time,
            'credit_card'   => [
                'type'            => $data['credit_card']['type'],
                'number'          => $data['credit_card']['number'],
                'name'            => $data['credit_card']['name'],
                'expiration_date' => $data['credit_card']['expirationDate'],
                'created_at'      => $time,
                'updated_at'      => $time,
            ]
        ];
    }
}
