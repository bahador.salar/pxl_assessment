<?php

namespace App\Services\FileImport\DataManipulators;

use App\Models\Account;

// this filter will prevent exact data duplications
class AccountUniquenessValidator extends BasedOnFieldManipulator
{
    public function __construct(string $attribute = 'uuid')
    {
        parent::__construct($attribute);
    }

    protected function isValid($value): bool
    {
        if(empty($value))
            return false;

        return !Account::query()->where('uuid', $value)->exists();
    }
}
