<?php

namespace App\Services\FileImport\DataManipulators;

// this requirement was in Bonus section in the assessment pdf
class CreditCardValidator extends BasedOnFieldManipulator
{
    public function __construct(string $attribute = 'credit_card.number')
    {
        parent::__construct($attribute);
    }

    /**
     * checks if credit card contains three of the same numbers consecutively
     *
     * @example 111 or 222 is ok, 123,1123,23344 is not ok
     *
     * @param $value
     * @return bool
     */
    protected function isValid($value): bool
    {
        if (empty($value))
            return false;

        // (.) captures a character.
        // \1 refers to the captured character.
        // so (.) captures a character and two \1 says I need 2 more
        return preg_match('/(.)\1\1/', $value) > 0;
    }
}
