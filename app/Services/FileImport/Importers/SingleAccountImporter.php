<?php

namespace App\Services\FileImport\Importers;

use App\Models\Account;
use Illuminate\Support\Arr;
use App\Services\FileImport\Contracts\ImporterContract;

/**
 * this Importer will throw timeout error on large files (Most definitely the execution time will pass and throw timeout error)
 */
class SingleAccountImporter implements ImporterContract
{
    /**
     * @var array
     */
    private $accountData;

    /**
     * @var array
     */
    private $creditCardData;

    public function prepare(array $data): ImporterContract
    {
        $this->accountData = Arr::except($data, ['credit_card']);
        $this->creditCardData = $data['credit_card'];

        return $this;
    }

    public function import(bool $force): void
    {
        /** @var Account $account */
        $account = Account::query()->create($this->accountData);

        $account->creditCards()->create($this->creditCardData);
    }
}
