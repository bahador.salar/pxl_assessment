<?php

namespace App\Services\FileImport\Importers;

use App\Models\Account;
use App\Models\CreditCard;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use App\Services\FileImport\Contracts\ImporterContract;

/**
 * this Importer is good for large files (max execution time is below 20 seconds for 2.5 mb file)
 * max execution time will be below 10 seconds if all filters are applied
 */
class BulkAccountImporter implements ImporterContract
{
    /**
     * bulk size of data import
     * its public so that it can be modified from outside of class
     * @var int
     */
    public static $bulkSize = 100;

    private $importableDataCount = 0;

    private $accountsData = [];

    private $creditCardsData = [];

    public function prepare(array $data): ImporterContract
    {
        $this->accountsData[] = Arr::except($data, ['credit_card']);
        $this->creditCardsData[] = $data['credit_card'];

        $this->importableDataCount++;

        return $this;
    }

    public function import(bool $force): void
    {
        if ($this->importableDataCount < static::$bulkSize && !$force)
            return;

        /** in case $force set to true check if data is insertable */
        if (empty($this->accountsData) || empty($this->creditCardsData))
            return;

        DB::beginTransaction();

        try {
            $accountId = Account::query()->sharedLock()->max('id') + 1;

            Account::query()->insert($this->accountsData);

            $relations = [];
            foreach ($this->creditCardsData as $creditCard) {
                $relations[] = array_merge([
                    'account_id' => $accountId,
                ], $creditCard);

                $accountId++;
            }

            CreditCard::query()->insert($relations);

            DB::commit();

            $this->reset();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    private function reset()
    {
        $this->importableDataCount = 0;
        $this->accountsData = [];
        $this->creditCardsData = [];
    }

}
