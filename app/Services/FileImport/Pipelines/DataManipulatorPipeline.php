<?php

namespace App\Services\FileImport\Pipelines;

use App\Services\FileImport\Contracts\DataManipulatorContract;
use App\Services\FileImport\Contracts\DataManipulatorPipelineContract;

class DataManipulatorPipeline implements DataManipulatorPipelineContract
{
    /** @var DataManipulatorContract[] */
    private $manipulators = [];

    /**
     * @var DataManipulatorContract
     */
    private $applied;

    /**
     * @param DataManipulatorContract $manipulator
     * @return DataManipulatorPipeline
     */
    public function addManipulator(DataManipulatorContract $manipulator): DataManipulatorPipelineContract
    {
        $this->manipulators[] = $manipulator;

        return $this;
    }

    public function apply(?array $data): ?array
    {
        if(empty($this->manipulators))
            return $data;

        foreach ($this->manipulators as $manipulator){
            $data = $manipulator->handle($data);

            if(is_null($data))
                $this->applied = $manipulator;
        }

        return $data;
    }

    public function getApplied(): ?DataManipulatorContract
    {
       return $this->applied;
    }
}
