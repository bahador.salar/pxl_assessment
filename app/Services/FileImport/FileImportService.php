<?php

namespace App\Services\FileImport;

use App\Services\FileImport\Contracts\ImporterContract;
use App\Services\FileRead\Contracts\FileReaderContract;
use App\Services\FileImport\Contracts\DataManipulatorPipelineContract;

class FileImportService
{
    /**
     * @var FileReaderContract
     */
    private $reader;

    /**
     * @var ImporterContract
     */
    private $importer;

    /**
     * @var DataManipulatorPipelineContract|null
     */
    private $dataManipulator;

    public function __construct(
        FileReaderContract              $reader,
        ImporterContract                $importer,
        DataManipulatorPipelineContract $dataManipulator = null
    )
    {
        $this->reader = $reader;
        $this->importer = $importer;
        $this->dataManipulator = $dataManipulator;
    }

    public function import(): void
    {
        foreach ($this->reader->read() as $data) {
            if ($this->dataManipulator)
                $data = $this->dataManipulator->apply($data);

            if (!empty($data))
                $this->importer->prepare($data)->import(false);
        }

        // this line is force to import the last bit of data if remained
        $this->importer->import(true);
    }
}
