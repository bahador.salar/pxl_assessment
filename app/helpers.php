<?php

use Carbon\Carbon;
use Ramsey\Uuid\Uuid;

if (!function_exists('parseToCarbon')) {
    /**
     * @param string|null $date
     * @return Carbon|null
     */
    function parseToCarbon(?string $date): ?Carbon
    {
        if (empty($date))
            return null;

        if (preg_match('/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/', $date)) {
            $carbon = Carbon::createFromFormat('d/m/Y', $date);

            return $carbon ?: null;
        }

        return Carbon::parse($date);
    }
}

if (!function_exists('array_keys_exists')) {
    /**
     * Check the keys of an array against a list of values. Returns true if all values in the list
     * is not in the array as a key. Returns false otherwise.
     *
     * @param $array Array associative array with keys and values
     * @param $mustHaveKeys Array whose values contain the keys that MUST exist in $array
     * @param &$missingKeys Array. Pass by reference. An array of the missing keys in $array as string values.
     * @return Boolean. Return true only if all the values in $mustHaveKeys appear in $array as keys.
     */
    function array_keys_exists(array $array, array $mustHaveKeys, array &$missingKeys = []): bool
    {
        // extract the keys of $array as an array
        $keys = array_keys($array);

        // ensure the keys we look for are unique
        $mustHaveKeys = array_unique($mustHaveKeys);

        // $missingKeys = $mustHaveKeys - $keys
        // we expect $missingKeys to be empty if all goes well
        $missingKeys = array_diff($mustHaveKeys, $keys);

        return empty($missingKeys);
    }
}

if (!function_exists('fileItemUuid')) {

    /**
     * created an uuid based on an array
     *
     * @param array|null $array
     * @return string|null
     */
    function fileItemUuid(?array $array): ?string
    {
        if (empty($array))
            return null;

        return Uuid::uuid3(Uuid::NAMESPACE_OID, json_encode($array))->toString();
    }
}

