<?php

namespace App\Console\Commands;

use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use App\Jobs\AccountsJsonFileImportJob;

class ImportChallengeJsonCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'system:import:challenge-json';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports the challenge json file into database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $start = Carbon::now();

        $this->line('started_at: ' . $start->format('Y-m-d H:i:s'));

        // If the queue set to queueable drivers like redis or memcache
        // the process will be executed in background and needs a worker to run
        // otherwise it will be executed in sync mode
        AccountsJsonFileImportJob::dispatch();

        $end = Carbon::now();

        $this->line('ended_at: ' . $end->format('Y-m-d H:i:s'));

        $this->outputExecutionTime($start, $end);

        return 1;
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     */
    private function outputExecutionTime(Carbon $start, Carbon $end): void
    {
        $seconds = $start->diffInSeconds($end) . 's';
        $milliseconds = $start->diffInMilliseconds($end) . 'ms';

        $this->info('execution time: [' . implode(', ', [$seconds, $milliseconds]) . ']');
    }
}
