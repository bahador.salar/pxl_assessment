<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CreditCard extends Model
{
    use HasFactory;

    protected $fillable = [
        'account_id',
        'type',
        'number',
        'name',
        'expiration_date'
    ];

    public function account(): BelongsTo
    {
        return $this->belongsTo(Account::class);
    }
}
