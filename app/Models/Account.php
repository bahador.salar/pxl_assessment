<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'address',
        'checked',
        'description',
        'interest',
        'date_of_birth',
        'email',
        'account',
        'identifier',
    ];

    protected $casts = [
        'checked' => 'bool',
    ];

    protected $dates = [
        'date_of_birth'
    ];

    public function creditCards(): HasMany
    {
        return $this->hasMany(CreditCard::class);
    }
}
