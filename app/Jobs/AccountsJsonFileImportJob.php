<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\FileRead\Readers\JsonReader;
use App\Services\FileRead\FilerReaderService;
use App\Services\FileRead\Contracts\PointerContract;
use App\Services\FileImport\DataManipulators\AgeValidator;
use App\Services\FileImport\FileImportService;
use App\Services\FileRead\Pointers\FileCachePointer;
use App\Services\FileImport\DataManipulators\CreditCardValidator;
use App\Services\FileImport\Pipelines\DataManipulatorPipeline;
use App\Services\FileRead\Contracts\FileReaderContract;
use App\Services\FileImport\Contracts\ImporterContract;
use App\Services\FileImport\DataManipulators\AccountUniquenessValidator;
use App\Services\FileImport\Importers\BulkAccountImporter;
use App\Services\FileImport\DataManipulators\AccountJsonDataManipulator;
use App\Services\FileImport\Contracts\DataManipulatorPipelineContract;

class AccountsJsonFileImportJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * orders file reader to continue from where it left off or read from beginning
     *
     * @var bool
     */
    private $shouldContinue;

    public function __construct(bool $shouldContinue = true)
    {
        $this->shouldContinue = $shouldContinue;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $importerService = new FileImportService(
            $this->getFileReader(),
            $this->getImporter(),
            $this->getDataManipulator()
        );

        $importerService->import();
    }

    private function getFileReader(): FileReaderContract
    {
        $readerStrategy = new JsonReader($this->getPath(), $this->getPointer());

        return new FilerReaderService($readerStrategy);
    }

    private function getPath(): string
    {
        return storage_path('app/importable/challenge.json');
    }

    private function getPointer(): PointerContract
    {
        return FileCachePointer::init(true);
    }

    private function getImporter(): ImporterContract
    {
        return new BulkAccountImporter();
    }

    private function getDataManipulator(): DataManipulatorPipelineContract
    {
        return (new DataManipulatorPipeline())
            ->addManipulator(new AccountJsonDataManipulator)
            ->addManipulator(new AccountUniquenessValidator)
            ->addManipulator(new AgeValidator)
            ->addManipulator(new CreditCardValidator);
    }

}
