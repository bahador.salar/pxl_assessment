<?php

use App\Models\Account;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreditCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // we can use foreign keys, but it prevents us from bulk inserting the data
        // so an indexed unsigned biginteger is preferred here for Optimal data import
        // The foreign key can be added later after data import with this code :
        // $table->foreignIdFor(Account::class)->constrained()->onDelete('cascade');

        Schema::create('credit_cards', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('account_id')->index();
            $table->string('type');
            $table->string('number');
            $table->string('name');
            $table->string('expiration_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_cards');
    }
}
